package model;

public class FullDepositorName {
    private String depositorName;
    private String depositorSurname;

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getDepositorSurname() {
        return depositorSurname;
    }

    public void setDepositorSurname(String depositorSurname) {
        this.depositorSurname = depositorSurname;
    }

    @Override
    public String toString() {
        return "FullDepositorName{" +
                "depositorName='" + depositorName + '\'' +
                ", depositorSurname='" + depositorSurname + '\'' +
                '}';
    }


}
