package model;

import model.enums.TypeOfTimeConstraints;

public class TimeConstraints {
    private int amount;
    private TypeOfTimeConstraints typeOfTimeConstraints;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public TypeOfTimeConstraints getTypeOfTimeConstraints() {
        return typeOfTimeConstraints;
    }

    public void setTypeOfTimeConstraints(TypeOfTimeConstraints typeOfTimeConstraints) {
        this.typeOfTimeConstraints = typeOfTimeConstraints;
    }

    @Override
    public String toString() {
        return "TimeConstraints{" +
                "amount=" + amount +
                ", typeOfTimeConstraints=" + typeOfTimeConstraints +
                '}';
    }
}
