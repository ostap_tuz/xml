package model.enums;

public enum TypeOfTimeConstraints {
    MONTHS, YEARS
}
