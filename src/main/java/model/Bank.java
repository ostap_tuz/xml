package model;

import model.enums.Type;

public class Bank {
    private int depositId;
    private String name;
    private String country;
    private Type type;
    private FullDepositorName fullDepositorName;
    private long accountId;
    private double amountOnDeposit;
    private int profitability;
    private TimeConstraints timeConstraints;

    public int getDepositId() {
        return depositId;
    }

    public void setDepositId(int depositId) {
        this.depositId = depositId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public FullDepositorName getFullDepositorName() {
        return fullDepositorName;
    }

    public void setFullDepositorName(FullDepositorName fullDepositorName) {
        this.fullDepositorName = fullDepositorName;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public double getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(double amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public int getProfitability() {
        return profitability;
    }

    public void setProfitability(int profitability) {
        this.profitability = profitability;
    }

    public TimeConstraints getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(TimeConstraints timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "depositId=" + depositId +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", type=" + type +
                ", fullDepositorName=" + fullDepositorName +
                ", accountId=" + accountId +
                ", amountOnDeposit=" + amountOnDeposit +
                ", profitability=" + profitability +
                ", timeConstraints=" + timeConstraints +
                "}\n";
    }
}
