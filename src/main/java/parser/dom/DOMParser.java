package parser.dom;

import model.Bank;
import model.FullDepositorName;
import model.TimeConstraints;
import model.enums.Type;
import model.enums.TypeOfTimeConstraints;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMParser {
    public static List<Bank> parse(Document document)
    {
        List<Bank> bankList = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("bank");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Bank bank = new Bank();

            Node node = nodeList.item(i);
            Element element = (Element)node;

            bank.setDepositId(Integer.parseInt(element.getAttribute("depositId")));
            bank.setName(element.getElementsByTagName("name").item(0).getTextContent());
            bank.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
            bank.setAccountId(Long.parseLong(element.getElementsByTagName("accountId").item(0).getTextContent()));
            bank.setAmountOnDeposit(Double.parseDouble(element.getElementsByTagName("amountOnDeposit").item(0).getTextContent()));
            bank.setProfitability(Integer.parseInt(element.getElementsByTagName("profitability").item(0).getTextContent()));
            bank.setType(Type.valueOf(element.getElementsByTagName("type").item(0).getTextContent().toUpperCase()));


            FullDepositorName fullDepositorName = getFullDepositorName(element.getElementsByTagName("depositor"));
            bank.setFullDepositorName(fullDepositorName);

            TimeConstraints timeConstraints = getTimeConstraints(element.getElementsByTagName("timeConstraints"));
            bank.setTimeConstraints(timeConstraints);

            bankList.add(bank);
        }

        return bankList;
    }

    public static FullDepositorName getFullDepositorName(NodeList nodeList)
    {
        FullDepositorName fullDepositorName = new FullDepositorName();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE)
        {
            Element element = (Element) nodeList.item(0);
            fullDepositorName.setDepositorName(element.getElementsByTagName("depositorName").item(0).getTextContent());
            fullDepositorName.setDepositorSurname(element.getElementsByTagName("depositorSurname").item(0).getTextContent());
        }
        return fullDepositorName;
    }

    public static TimeConstraints getTimeConstraints(NodeList nodeList)
    {
        TimeConstraints timeConstraints = new TimeConstraints();

        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE)
        {
            Element element = (Element)nodeList.item(0);
            try {
                timeConstraints.setAmount(Integer.parseInt(element.getElementsByTagName("months").item(0).getTextContent()));
                timeConstraints.setTypeOfTimeConstraints(TypeOfTimeConstraints.MONTHS);
            }catch (Exception e)
            {
                timeConstraints.setAmount(Integer.parseInt(element.getElementsByTagName("years").item(0).getTextContent()));
                timeConstraints.setTypeOfTimeConstraints(TypeOfTimeConstraints.YEARS);
            }
        }
        return timeConstraints;
    }

}
