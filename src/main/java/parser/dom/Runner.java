package parser.dom;

import model.Bank;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import xmlValidator.XMLValidator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Runner {
    private static DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder documentBuilder;
    public static void main(String[] args) {

        File XMLFile = new File("src\\main\\resources\\banks.xml");
        File XSDFile = new File("src\\main\\resources\\banksXSD.xsd");

        Document document = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(XMLFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        if (XMLValidator.validate(document, XSDFile))
        {
            List<Bank> banks = DOMParser.parse(document);
            System.out.println(banks);
        }else System.out.println("Fail in XML validation");
    }
}
