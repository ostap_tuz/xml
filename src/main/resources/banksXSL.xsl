<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


    <xsl:template match="/">
        <html>
            <body>
                <h1 style="color=red;">Bank deposits</h1>
                <table border="2">
                    <tr style="background-color:#57f15f">
                        <th>Deposit ID</th>
                        <th>Name</th>
                        <th>Country</th>
                        <th>Type</th>
                        <th>Depositor</th>
                        <th>Account Id</th>
                        <th>Amount on deposit</th>
                        <th>Profitability</th>
                        <th>Time constraints</th>
                    </tr>

                    <xsl:for-each select="banks/bank">
                        <tr>
                            <td><xsl:value-of select="@depositId"/></td>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="country"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td>
                                <xsl:value-of select="depositor/depositorName"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="depositor/depositorSurname"/>
                            </td>
                            <td><xsl:value-of select="accountId"/></td>
                            <td>
                                <xsl:value-of select="amountOnDeposit"/>
                                <xsl:text> USD</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="profitability"/>
                                <xsl:text> %</xsl:text>
                            </td>
                            <xsl:choose>
                                <xsl:when test="timeConstraints/years!='0'">
                                    <td>
                                        <xsl:value-of select="timeConstraints/years"/>
                                        <xsl:text> years </xsl:text>
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td>
                                        <xsl:value-of select="timeConstraints/months"/>
                                        <xsl:text> months </xsl:text>
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>

                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>